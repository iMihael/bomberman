angular.module('bomb', [
    'ngResource'
])
    .run([function () {
        window.socket = io();
    }])
    .directive('bindKeyboard', function ($document) {
        return function (scope, element, attrs) {
            $document.bind("keydown", function (event) {
                scope.game.key(event);
                event.preventDefault();
            });
        };
    })
    .factory('gameSrv', function ($resource) {
        return $resource('/rest', {}, {
            map: {
                url: '/rest/map/:id'
            },
            move: {
                method: 'POST',
                url: '/rest/move/:id'
            },
            bomb: {
                url: '/rest/bomb/:id'
            }
        });
    })
    .controller('gameCtrl', ['$scope', 'gameSrv', function ($scope, gameSrv) {

        var isNum = function (num) {
            return !isNaN(parseInt(num))
        };

        var up = 38,
            down = 40,
            left = 37,
            right = 39,
            space = 32;
        var actions = {};
        actions[up] = function () {
            $scope.game.movePlayer('up')
        };
        actions[down] = function () {
            $scope.game.movePlayer('down')
        };
        actions[left] = function () {
            $scope.game.movePlayer('left')
        };
        actions[right] = function () {
            $scope.game.movePlayer('right')
        };
        actions[space] = function () {
            $scope.game.placeBomb()
        };


        $scope.game = {
            userId: null,
            number: null,
            playerImg: null,
            position: null,
            surface: null,
            map: null,
            blockSize: 60,
            bombs: {},
            key: function (e) {
                if (actions.hasOwnProperty(e.keyCode)) {
                    actions[e.keyCode]();
                }
            },
            placeBomb: function () {
                gameSrv.bomb({id: $scope.game.userId});
            },
            renderBomb: function (x, y) {
                return $scope.game.surface.image('/img/bomb.png', 10 + x * $scope.game.blockSize, 10 + y * $scope.game.blockSize, $scope.game.blockSize - 20, $scope.game.blockSize - 20);
            },
            movePlayer: function (to) {
                var x = $scope.game.position.x;
                var y = $scope.game.position.y;

                if(to == 'up') {
                    y -= 1;
                } else if(to == 'left') {
                    x -= 1;
                } else if(to == 'right') {
                    x += 1;
                } else if(to == 'down') {
                    y += 1;
                }

                if (x >= 0 && x < $scope.game.map.width && y >= 0 && $scope.game.map.height > y) {
                    gameSrv.move({id: $scope.game.userId}, {
                        to: to
                    }, function () {
                        $scope.game.playerImg.animate({
                            x: 10 + x * $scope.game.blockSize,
                            y: 10 + y * $scope.game.blockSize
                        }, 250, function(){

                        });

                        $scope.game.position.x = x;
                        $scope.game.position.y = y;
                    });
                }
            },
            initPlayer: function () {
                $scope.game.playerImg = $scope.game.surface.image('/img/b' + $scope.game.number + '.png', 10, 10, $scope.game.blockSize - 20, $scope.game.blockSize - 20);
                $scope.game.movePlayer($scope.game.position.x, $scope.game.position.y);
            },
            initMap: function (map) {
                $scope.game.map = map;
                for (var i in map) {

                    if (isNum(i)) {

                        for (var j in map[i]) {

                            if (isNum(j)) {

                                var rect = $scope.game.surface.rect(
                                    i * $scope.game.blockSize,
                                    j * $scope.game.blockSize,
                                    $scope.game.blockSize,
                                    $scope.game.blockSize
                                );

                                if (map[i][j].hasOwnProperty('block')) {
                                    if (map[i][j]['block']['fixed']) {
                                        rect.attr({
                                            fill: '#8c8e8b'
                                        });
                                    } else {
                                        rect.attr({
                                            fill: '#b25600'
                                        });
                                    }
                                } else {
                                    rect.attr({
                                        fill: '#049605'
                                    });
                                }

                                map[i][j]['rect'] = rect;
                            }
                        }
                    }
                }
            },
            opponents: {},
            renderAllOpponents: function () {
                for (var i in $scope.game.opponents) {
                    var x = $scope.game.opponents[i].position.x;
                    var y = $scope.game.opponents[i].position.y;

                    $scope.game.opponents[i].img = $scope.game.playerImg = $scope.game.surface.image(
                        '/img/b' + i + '.png',
                        10 + x * $scope.game.blockSize,
                        10 + y * $scope.game.blockSize,
                        $scope.game.blockSize - 20, $scope.game.blockSize - 20
                    );
                }
            },
            moveOpponent: function (data) {

                var x = data.position.x;
                var y = data.position.y;

                if (!$scope.game.opponents.hasOwnProperty(data.number)) {
                    $scope.game.opponents[data.number] = data;
                    $scope.game.opponents[data.number].img = $scope.game.surface.image(
                        '/img/b' + data.number + '.png',
                        10 + x * $scope.game.blockSize,
                        10 + y * $scope.game.blockSize,
                        $scope.game.blockSize - 20, $scope.game.blockSize - 20
                    );
                } else {
                    $scope.game.opponents[data.number].img.animate({
                        x: 10 + x * $scope.game.blockSize,
                        y: 10 + y * $scope.game.blockSize
                    }, 250);
                }
            }
        };

        $scope.game.surface = Snap("#game");

        window.socket.on('login', function (data) {
            if (data) {
                $scope.game.userId = data.id;
                $scope.game.position = data.position;
                $scope.game.number = data.number;
                $scope.game.opponents = data.opponents;


                gameSrv.map({id: $scope.game.userId}, function (map) {
                    if (map) {
                        $scope.game.initMap(map);

                        $scope.game.renderAllOpponents();

                        $scope.game.initPlayer();


                        window.socket.on('move', function (data) {
                            $scope.game.moveOpponent(data);
                        });

                        window.socket.on('logout', function (data) {
                            if ($scope.game.opponents.hasOwnProperty(data.number)) {
                                $scope.game.opponents[data.number].img.remove();
                                delete $scope.game.opponents[data.number];
                            }

                            if($scope.game.number == data.number) {
                                $scope.game.playerImg.remove();
                                document.write('You loose!');
                            }
                        });

                        window.socket.on('bomb', function (data) {
                            $scope.game.bombs[data.placed] = data;
                            $scope.game.bombs[data.placed].img = $scope.game.renderBomb(data.position.x, data.position.y);
                        });

                        window.socket.on('explode', function (bomb) {
                            var explosionRadius = 3;

                            var drawFile = function (x, y) {

                                if ($scope.game.map.hasOwnProperty(x) && $scope.game.map[x].hasOwnProperty(y)) {
                                    var fire = $scope.game.surface.rect(10 + x * $scope.game.blockSize,
                                        10 + y * $scope.game.blockSize,
                                        $scope.game.blockSize - 20, $scope.game.blockSize - 20)
                                        .attr({
                                            fill: '#ff0000'
                                        });

                                    setTimeout(function(){
                                        fire.remove();
                                    }, 250);

                                    if ($scope.game.map[x][y].hasOwnProperty('block')) {
                                        if (!$scope.game.map[x][y]['block']['fixed']) {
                                            $scope.game.map[x][y]['rect'].attr({
                                                fill: '#049605'
                                            });
                                            delete $scope.game.map[x][y]['block'];
                                        } else {
                                            fire.remove();
                                        }

                                        return false;
                                    }

                                    return true;
                                }


                            };

                            for (var i = bomb.position.x; i <= bomb.position.x + explosionRadius; i++) {
                                if (!drawFile(i, bomb.position.y)) {
                                    break;
                                }
                            }

                            for (var i = bomb.position.x; i >= bomb.position.x - explosionRadius; i--) {
                                if (!drawFile(i, bomb.position.y)) {
                                    break;
                                }
                            }

                            for (var i = bomb.position.y; i >= bomb.position.y - explosionRadius; i--) {
                                if (!drawFile(bomb.position.x, i)) {
                                    break;
                                }
                            }

                            for (var i = bomb.position.y; i <= bomb.position.y + explosionRadius; i++) {
                                if (!drawFile(bomb.position.x, i)) {
                                    break;
                                }
                            }

                            $scope.game.bombs[bomb.placed].img.remove();
                            delete $scope.game.bombs[bomb.placed];
                        });
                    }
                });
            }
        });

        window.socket.emit('login');


    }]);