var gulp = require('gulp'),
    concat = require('gulp-concat');

var dev = false;

var src = [
    './node_modules/angular/angular.min.js',
    './node_modules/angular-resource/angular-resource.min.js',
    './node_modules/snapsvg/dist/snap.svg-min.js',
    './node_modules/socket.io-client/socket.io.js',
    './public/js/src/**/*.js'
];

if(dev) {
    gulp.task('js', function () {
        gulp.src(src)
            .pipe(concat('app.js'))
            .pipe(gulp.dest('./public/js'))
    });

    gulp.task('default', ['js'], function () {
        gulp.watch('./public/js/src/**/*.js', ['js']);
    });
} else {
    gulp.task('default', function () {
        gulp.src(src)
            .pipe(concat('app.js'))
            .pipe(gulp.dest('./public/js'))
    });
}
