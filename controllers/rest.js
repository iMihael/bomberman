var game = require('../components/game');

module.exports = {
    map: function(req, res){
        var map = game.getMap(req.params.id);
        if(map) {
            res.json(map);
        } else {
            res.status(422);
            res.end();
        }
    },
    bomb: function(req, res) {
        var map = game.getMap(req.params.id);
        var user = game.getUser(req.params.id);
        var x = user.position.x;
        var y = user.position.y;

        if(map.hasOwnProperty(x) && map[x].hasOwnProperty(y)) {
            if (user && !map[x][y].hasOwnProperty('block')) {

                var bomb = game.placeBomb(x, y, user);
                if(bomb) {
                    game.tellBomb(user, bomb);
                    //res.json(bomb);
                    res.status(204);
                    res.end();
                } else {
                    res.status(422);
                    res.end();
                }
            } else {
                res.status(422);
                res.end();
            }
        }
    },
    move: function(req, res){
        var to = req.body.to;
        var map = game.getMap(req.params.id);
        var user = game.getUser(req.params.id);

        var x = user.position.x;
        var y = user.position.y;

        if(to == 'up') {
            y -= 1;
        } else if(to == 'left') {
            x -= 1;
        } else if(to == 'right') {
            x += 1;
        } else if(to == 'down') {
            y += 1;
        }

        if(map.hasOwnProperty(x) && map[x].hasOwnProperty(y)) {
            if (user && !map[x][y].hasOwnProperty('block') && !map[x][y].hasOwnProperty('bomb')) {
                user.position.x = x;
                user.position.y = y;
                game.tellPosition(user);
                res.status(204);
                res.end();
            } else {
                res.status(422);
                res.end();
            }
        }
    }
};