Bomberman
==================

REQUIREMENTS:
---------------
* node
* npm

INSTALL
-------------
* npm install
* ./node_modules/gulp/bin/gulp.js

Run
--------------
* Run with no bots: ./www
* Run with one or more bots: ./www --bots=1
* Visit http://localhost:8080 to enjoy the game
* To add bot during the game run: node ./bot/bot.js --port=8080