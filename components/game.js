var map = {},
    width = 13,
    height = 11,
    cornerAreas = [
        [0, 0],
        [0, 1],
        [1, 0],

        [width - 1, 0],
        [width - 2, 0],
        [width - 1, 1],

        [0, height - 1],
        [0, height - 2],
        [1, height - 1],

        [width - 1, height - 1],
        [width - 1, height - 2],
        [width - 2, height - 1]
    ],
    players = {
        users: {},
        current: 0,
        maximum: 4
    },
    initialPosition = {
        1: {
            "x": 0,
            "y": 0
        },
        2: {
            "x": width - 1,
            "y": 0
        },
        3: {
            "x": 0,
            "y": height - 1
        },
        4: {
            "x": width - 1,
            "y": height - 1
        }
    },
    bombExplosion = 3000,
    explosionRadius = 3,
    bombsMaximum = 1;

var getRandomInt = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

var makeId = function () {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
        text += possible.charAt(getRandomInt(0, possible.length));

    return text;
};

var incrementBombs = function () {
    bombsMaximum++;
    setTimeout(incrementBombs, 30000);
};

var game = {
    bombExplode: function (bomb, user, map) {

        for (var j in players.users) {
            players.users[j].socket.emit('explode', bomb);
        }
        user.bombs--;

        var explodeArea = function (x, y, map) {
            if (map.hasOwnProperty(x) && map[x].hasOwnProperty(y)) {
                if (map[x][y].hasOwnProperty('block')) {
                    if (!map[x][y]['block']['fixed']) {
                        delete map[x][y]['block'];
                    }
                    return false;
                }

                var outUsers = [];
                for (var i in players.users) {
                    if (players.users[i].position.x == x && players.users[i].position.y == y) {
                        outUsers.push(players.users[i]);
                    }
                }
                if (outUsers.length > 0) {
                    do {
                        var user = outUsers.pop();
                        game.logOut(user.id);
                    } while (outUsers.length > 0);
                }
            }

            return true;
        };

        for (var i = bomb.position.x; i <= bomb.position.x + explosionRadius; i++) {
            if (!explodeArea(i, bomb.position.y, map)) {
                break;
            }
        }

        for (var i = bomb.position.x; i >= bomb.position.x - explosionRadius; i--) {
            if (!explodeArea(i, bomb.position.y, map)) {
                break;
            }
        }

        for (var i = bomb.position.y; i >= bomb.position.y - explosionRadius; i--) {
            if (!explodeArea(bomb.position.x, i, map)) {
                break;
            }
        }

        for (var i = bomb.position.y; i <= bomb.position.y + explosionRadius; i++) {
            if (!explodeArea(bomb.position.x, i, map)) {
                break;
            }
        }


        delete map[bomb.position.x][bomb.position.y]['bomb'];
        delete bomb;


    },
    tellBomb: function (user, bomb) {
        for (var i in players.users) {
            if (players.users[i].hasOwnProperty('socket')) {
                players.users[i].socket.emit('bomb', bomb);
            }
        }
    },
    placeBomb: function (x, y, user) {
        if (user.bombs < bombsMaximum) {
            var placed = Date.now();
            var bomb = {
                placed: placed,
                position: {
                    x: x,
                    y: y
                }
            };
            map[x][y]['bomb'] = bomb;
            user.bombs++;

            setTimeout(function () {
                game.bombExplode(bomb, user, map);
            }, bombExplosion);

            return bomb;
        } else {
            return false;
        }
    },
    tellPosition: function (user) {
        for (var i in players.users) {
            if (players.users[i].hasOwnProperty('socket') && user.number != players.users[i].number) {
                players.users[i].socket.emit('move', {
                    number: user.number,
                    position: user.position
                });
            }
        }
    },
    getUser: function (id) {
        if (players.users.hasOwnProperty(id)) {
            return players.users[id];
        }
        return false;
    },
    initMap: function () {
        for (var i = 0; i < width; i++) {
            map[i] = {};
            for (var j = 0; j < height; j++) {
                map[i][j] = {};

                if (i % 2 && j % 2) {
                    map[i][j]['block'] = {
                        fixed: true
                    };
                } else {
                    //random blocks
                    if (getRandomInt(0, 1)) {
                        map[i][j]['block'] = {
                            fixed: false
                        };
                    }
                }
            }
        }

        //clear corner blocks
        for (var i = 0; i < cornerAreas.length; i++) {
            var area = cornerAreas[i];
            map[area[0]][area[1]] = {};
        }

        map.width = width;
        map.height = height;

        setTimeout(incrementBombs, 30000);

        return map;
    },
    logOut: function (id) {
        if (players.users.hasOwnProperty(id)) {

            for (var i in players.users) {
                if (players.users[i].hasOwnProperty('socket')) {
                    players.users[i].socket.emit('logout', {
                        number: players.users[id].number
                    });
                }
            }

            delete players.users[id];
            players.current--;

            return true;
        }
        return false;
    },
    logInGame: function (socket) {
        if (players.current < players.maximum) {

            var id = makeId();
            players.current++;

            var numbers = [1, 2, 3, 4];
            var freeNumber;
            for (var j = 0; j < numbers.length; j++) {
                var add = true;
                for (var i in players.users) {
                    if (players.users[i].number == numbers[j]) {
                        add = false;
                        break;
                    }
                }
                if (add) {
                    freeNumber = numbers[j];
                    break;
                }
            }

            players.users[id] = {
                id: id,
                position: {
                    x: initialPosition[freeNumber].x,
                    y: initialPosition[freeNumber].y
                },
                number: freeNumber,
                socket: socket,
                bombs: 0
            };

            var opponents = {};

            for (var i in players.users) {
                if (players.users[i].hasOwnProperty('position') && i != id) {
                    opponents[players.users[i].number] = {
                        position: players.users[i].position,
                        number: players.users[i].number
                    };
                }
            }

            game.tellPosition(players.users[id]);

            return {
                id: id,
                position: players.users[id].position,
                number: players.users[id].number,
                opponents: opponents
            }
        } else {
            return false;
        }
    },
    getMap: function (id) {
        if (players.users.hasOwnProperty(id)) {
            return map;
        } else {
            return false;
        }

    }
};

module.exports = game;