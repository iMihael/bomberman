var io;
var game = require('./game');

module.exports = {
    init: function(server){
        io = require('socket.io')(server);

        io.on('connection', function(sock){

            sock.on('login', function(){
                var userData = game.logInGame(this);
                if(userData) {
                    this['userId'] = userData.id;
                    this.emit('login', userData);
                } else {
                    this.emit('login', false);
                }
            });

            sock.on('disconnect', function(){
                if(this.hasOwnProperty('userId')) {
                    game.logOut(this.userId);
                }
            });
        });
    }
};