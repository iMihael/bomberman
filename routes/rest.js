var router = require('express').Router();
var rest = require('../controllers/rest');

router.get('/map/:id', rest.map);
router.get('/bomb/:id', rest.bomb);

var limits = {};
var limiter = function(req, res, next) {
    //simple rate limiter by user id

    var id = req.params.id;
    var maxHits = 1;
    var expire = 250; //seconds
    var now = Date.now();

    if(!limits.hasOwnProperty(id)) {
        limits[id] = {
            remaining: maxHits,
            reset: now + expire
        };
        next();
    } else {
        if (now > limits[id].reset) {
            limits[id].reset = now + expire;
            limits[id].remaining = maxHits;
            next();
        } else if(limits[id].remaining > 1) {
            limits[id].remaining--;
            next();
        } else {
            res.status(429).send('Rate limit exceeded');
        }
    }
};

router.post('/move/:id', limiter, rest.move);


module.exports = router;