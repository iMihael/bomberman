/**
 * Created by mihael on 24.03.16.
 */

var router = require('express').Router();
var site = require('../controllers/site');

router.get('/', site.index);

module.exports = router;