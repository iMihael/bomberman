var randomOpponent,
    path,
    emptyPath;

var pathfind = require('./pathfind'),
    rest = require('./rest');

var game = {
    id: null,
    number: null,
    position: null,
    map: null,
    opponents: {}
};

var lastBomb = Date.now();

var gameModule = {
    placeBomb: function(id) {
        // var now = Date.now();
        //
        // if(now - lastBomb > 3000) {

            rest.bomb(id, function () {
                setTimeout(gameModule.cycle, 250);
            }, function () {
                setTimeout(gameModule.cycle, 250);
            });
            // lastBomb = now;
        // } else {
        //     setTimeout(gameModule.cycle, 250);
        // }
    },
    cycle: function(){

        if(!game.opponents.hasOwnProperty(randomOpponent)) {
            setTimeout(gameModule.begin, 1000);
            return;
        }

        var step, to;

        //check if in bomb area
        if(pathfind.inBombArea(game.position.x, game.position.y)) {
            path = pathfind.findSafePlace(game.position.x, game.position.y, game.map);
            path.shift();
            step = path.shift();
        } else {

            path = pathfind.find(game.position, game.opponents[randomOpponent].position);

            if (path.length > 0) {
                path.shift();
                path.pop();

                if (path.length > 0) {
                    step = path.shift();
                    //check if step not in bomb area
                    if(pathfind.inBombArea(step[0], step[1])) {
                        setTimeout(gameModule.cycle, 250);
                        return;
                    }
                } else {
                    //place bomb
                    gameModule.placeBomb(game.id);
                }
            } else {
                emptyPath = pathfind.findEmpty(game.position, game.opponents[randomOpponent].position);
                emptyPath.shift();
                emptyPath.pop();

                step = emptyPath.shift();
                //check if step not in bomb area

                if(pathfind.inBombArea(step[0], step[1])) {
                    setTimeout(gameModule.cycle, 250);
                    return;
                }
            }
        }

        if(step) {
            if (step[0] > game.position.x) {
                to = 'right';
            } else if (step[0] < game.position.x) {
                to = 'left';
            } else if (step[1] > game.position.y) {
                to = 'down';
            } else if (step[1] < game.position.y) {
                to = 'up';
            }

            rest.move(game.id, to, function () {
                game.position.x = step[0];
                game.position.y = step[1];

                setTimeout(gameModule.cycle, 250);
            }, function () {
                gameModule.placeBomb(game.id);
            });
        } else {
            setTimeout(gameModule.cycle, 250);
        }


    },
    getGame: function(){
        return game;
    },
    moveOpponent: function(data) {
        game.opponents[data.number] = data;
    },
    removeOpponent: function(data) {
        delete game.opponents[data.number];
    },
    begin: function(){
        var numbers = [];
        for(var i in game.opponents) {
            numbers.push(i);
        }

        if(numbers.length > 0) {
            randomOpponent = numbers[Math.floor(Math.random() * numbers.length)];

            setTimeout(gameModule.cycle, 250);

        } else {
            setTimeout(gameModule.begin, 1000);
        }
    }
};

module.exports = gameModule;