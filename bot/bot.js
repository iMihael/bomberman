process.argv.forEach(function (val, index, array) {
    var matches = val.match(/^--port=(\d+)$/);
    if (matches) {
        var port = matches[1];
        var address = 'http://localhost:' + port,
            socket = require('./sock'),
            rest = require('./rest');

        rest.init(address + '/rest');
        socket.init(address);
        socket.login();
    }
});







