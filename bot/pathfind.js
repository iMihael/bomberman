var PF = require('pathfinding'),
    game = require('./game'),
    emptyGrid,
    explosionRadius = 3,
    grid,
    bombMatrix = {},
    finder;

var pathfind = {
    init: function (map) {

        var matrix = [];
        var emptyMatrix = [];

        for (var y = 0; y < map.height; y++) {
            var row = [];
            var emptyRow = [];
            for (var x = 0; x < map.width; x++) {
                row.push(map[x][y].hasOwnProperty('block') ? 1 : 0);
                emptyRow.push((map[x][y].hasOwnProperty('block') && map[x][y]['block']['fixed']) ? 1 : 0);
            }

            matrix.push(row);
            emptyMatrix.push(emptyRow);
        }


        for (var i = 0; i < map.width; i++) {
            bombMatrix[i] = {};
            for (var j = 0; j < map.height; j++) {
                bombMatrix[i][j] = {};

                if (i % 2 && j % 2) {
                    bombMatrix[i][j]['block'] = {
                        fixed: true
                    };
                } else {
                    bombMatrix[i][j]['bomb'] = false;
                }
            }
        }

        emptyGrid = new PF.Grid(emptyMatrix);
        grid = new PF.Grid(matrix);

        finder = new PF.AStarFinder();
    },
    getGrid: function(){
        return grid;
    },
    findEmpty: function (src, dst) {
        var gridBackup = emptyGrid.clone();
        return finder.findPath(src.x, src.y, dst.x, dst.y, gridBackup);
    },
    find: function (src, dst) {
        var gridBackup = grid.clone();
        return finder.findPath(src.x, src.y, dst.x, dst.y, gridBackup);
    },
    setBombArea: function (x, y, map, state) {

        if (map.hasOwnProperty(x) && map[x].hasOwnProperty(y)) {
            if (map[x][y].hasOwnProperty('block')) {
                if(!state && !map[x][y]['block']['fixed']) {
                    grid.setWalkableAt(x, y, true);
                    delete map[x][y]['block'];
                }
                return false;
            }

            bombMatrix[x][y]['bomb'] = state;
        }

        return true;
    },
    updateBombGrid: function (x, y, map, state) {
        //var explosionRadius = 3;

        for (var i = x; i <= x + explosionRadius; i++) {
            if (!pathfind.setBombArea(i, y, map, state)) {
                break;
            }
        }

        for (var i = x; i >= x - explosionRadius; i--) {
            if (!pathfind.setBombArea(i, y, map, state)) {
                break;
            }
        }

        for (var i = y; i >= y - explosionRadius; i--) {
            if (!pathfind.setBombArea(x, i, map, state)) {
                break;
            }
        }

        for (var i = y; i <= y + explosionRadius; i++) {
            if (!pathfind.setBombArea(x, i, map, state)) {
                break;
            }
        }
    },
    setBomb: function (x, y, map) {
        pathfind.updateBombGrid(x, y, map, true);
        grid.setWalkableAt(x, y, false);
    },
    explodeBomb: function (x, y, map) {
        pathfind.updateBombGrid(x, y, map, false);
        grid.setWalkableAt(x, y, true);
    },
    inBombArea: function(x, y) {
        return bombMatrix.hasOwnProperty(x) && bombMatrix[x].hasOwnProperty(y) && bombMatrix[x][y].hasOwnProperty('bomb') && bombMatrix[x][y]['bomb'];
    },
    findSafePlace: function(x, y, map) {
        var points = [];

        for (var i = 0; i < map.width; i++) {
            for (var j = 0; j < map.height; j++) {
                if(!map[i][j].hasOwnProperty('block') && !pathfind.inBombArea(i, j)) {

                    var path = pathfind.find({x: x, y: y}, {x: i, y: j});

                    if(path.length > 0) {
                        points.push({
                            x: i,
                            y: j,
                            path: path,
                            length: path.length
                        });
                    }
                }
            }
        }

        if(points.length > 0) {

            points.sort(function (a, b) {
                if (a.length < b.length) {
                    return -1;
                } else if (a.length > b.length) {
                    return 1;
                } else {
                    return 0;
                }
            });

            return points[0].path;
        }

        return [];
    },
};


module.exports = pathfind;