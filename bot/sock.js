var sock;
var game = require('./game'),
    pathfind = require('./pathfind'),
    rest = require('./rest');

module.exports = {
    init: function(address) {
        sock = require('socket.io-client').connect(address);

        sock.on('logout', function(data){
            if(game.getGame().number == data.number) {
                process.exit();
            } else {
                game.removeOpponent(data);
            }
        });

        sock.on('login', function(data){
            for(var i in data) {
                game.getGame()[i] = data[i];
            }

            rest.getMap(game.getGame().id, function(data){
                game.getGame().map = data;
                pathfind.init(game.getGame().map);

                game.begin();
            });
        });

        sock.on('bomb', function(data){
            pathfind.setBomb(data.position.x, data.position.y, game.getGame().map);
        });

        sock.on('explode', function(data){
            pathfind.explodeBomb(data.position.x, data.position.y, game.getGame().map);
        });

        sock.on('move', function(data){
            game.moveOpponent(data);
        });

        return sock;
    },
    login: function(){
        sock.emit('login');
    }
};