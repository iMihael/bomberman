var Client = require('node-rest-client').Client,
    client = new Client();

var address;

module.exports = {
    init: function(_address) {
        address = _address;
    },
    getMap: function(id, success){
        client.get(address + '/map/' + id, function(data) {
            success(data);
        });
    },
    move: function(id, to, success, error) {
        client.post(address + '/move/' + id, {
            headers: { "Content-Type": "application/json" },
            data: { to: to }
        }, function(data, rsp){
            if(rsp.statusCode == 204) {
                success();
            } else {
                error();
            }
        });
    },
    bomb: function(id, success, error) {
        client.get(address + '/bomb/' + id, function(d, rsp) {
            if(rsp.statusCode == 204) {
                success();
            } else {
                error();
            }
        });
    }
};